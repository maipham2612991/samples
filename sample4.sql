-- A high earner in a department is an employee who has a salary in the top three unique salaries for that department.
-- Write an SQL query to find the employees who are high earners in each of the departments.

WITH topsalary AS (
  SELECT
  departmentId,
  MAX(salary) AS max_salary,
  MIN(salary) AS min_salary
  from (
    SELECT
      departmentId,
      salary,
      ROW_NUMBER() OVER(PARTITION BY departmentId ORDER BY salary desc) as rn
    FROM (
      SELECT
        distinct departmentId,salary
      FROM Employee
      ORDER BY departmentId, salary desc
    ) AS agg
  ) as agg1
  WHERE rn <= 3
  GROUP BY departmentId
)

SELECT
    d.name as Department,
    emp.name as Employee,
    emp.salary as Salary
FROM Employee AS emp
         INNER JOIN topsalary AS ts
                    ON emp.departmentId = ts.departmentId
         INNER JOIN Department as d
                    ON d.id = emp.departmentId
WHERE emp.salary >= min_salary AND emp.salary <= max_salary
