-- 1.  There are turn_data table and rating_conversations table. One conversation have multiple – turns,
-- this query agg features of each conversation including bot_response_len, dialog, – etc.. (Redshift)

SELECT
    r.conversation_id,
    COUNT(*) as turn_count,
    LISTAGG(t.bot_response_len, ', ') AS bot_response_len_list,
    LISTAGG(t.turn_duration, ', ') AS turn_duration_list,
    LISTAGG(t.dialog_act_intent_dialogactintent, ', ') AS dialog_act_intent_list,
    LISTAGG(t.dialog_act_intent_topic, ', ') AS dialog_act_intent_topic_list,
    LISTAGG(t.speak_output, '| ') AS speak_output_list,
    SUM(CASE WHEN t.follow_up_flow IS TRUE THEN 1 ELSE 0 END) AS follow_up_flow_count
FROM log_schema.rating_conversations as r
         INNER JOIN (
    SELECT
        conversation_id,
        turn_id,
        (length(bot_response) - length(replace(bot_response, ' ', '')) + 1) as bot_response_len,
        DATEDIFF(millisecond, start_timestamp::TIMESTAMP, end_timestamp::TIMESTAMP) as turn_duration,
        dialog_act_intent_dialogactintent,
        dialog_act_intent_topic,
        speak_output,
        follow_up_flow
    FROM log_schema.turn_data
) as t
                    ON r.conversation_id = t.conversation_id
GROUP BY r.conversation_id
