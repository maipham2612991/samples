-- 2. SQL query to find the prices of all products before 2019-08-16.
-- Assume the price of all products after  any change is 10 (mysql)
CREATE PROCEDURE ProductPrice(IN updateDate DATE)
BEGIN
SELECT
    prod.product_id,
    IFNULL(prod_rank.new_price, 10) as price
FROM
    (SELECT
         product_id,
         change_date,
         new_price,
         RANK() OVER(PARTITION BY product_id ORDER BY change_date DESC) as ranking
     FROM Products
     WHERE change_date <= updateDate) as prod_rank
        RIGHT JOIN
    (SELECT DISTINCT product_id
     FROM Products) as prod
    ON prod_rank.product_id = prod.product_id
WHERE prod_rank.ranking = 1 OR prod_rank.ranking IS NULL

END;

CALL ProductPrice('2019-08-16')
