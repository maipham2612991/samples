-- 3. Write a query to find the top 5 artists whose songs appear most frequently in the
-- Top 10 of the global_song_rank table. Display the top 5 artist names in ascending order,
-- along with their song appearance ranking.

SELECT
  a.artist_name,
  agg.artist_rank
FROM (
  SELECT
    song.artist_id,
    DENSE_RANK() OVER(ORDER BY COUNT(song.song_id) DESC) as artist_rank
  FROM global_song_rank as rk
  INNER JOIN songs as song
  ON rk.song_id = song.song_id
  WHERE rk.rank <= 10
  GROUP BY song.artist_id
) AS agg
INNER JOIN artists as a
  ON a.artist_id = agg.artist_id
WHERE artist_rank <= 5
ORDER BY artist_rank, a.artist_name
